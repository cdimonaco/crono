import React from 'react'
import styled from '@emotion/styled'
import {
  Typography,
  List,
  Divider,
  Hidden,
} from '@material-ui/core'
import { theme } from '../theme'
import CronoList from './List'

const OverviewContainer = styled.div`
  display: flex;
  flex-direction: column;
`
const OverviewCategoryContainer = styled.div`
  padding: ${theme.spacings.sm};
`

const ListsOverview: React.FC = () => (
  <OverviewContainer>
    <OverviewCategoryContainer>
      <Hidden mdUp>
        <Typography variant='h6'>Recent Lists</Typography>
      </Hidden>
      <Hidden smDown>
        <Typography variant='h3'>Recent Lists</Typography>
      </Hidden>
    </OverviewCategoryContainer>
    <List>
      {['Fosdem 2020', 'Fosdem 2020', 'Budget feb', 'Budget Marzo', 'Home'].map(
        listName => (
          <>
            <CronoList
              listName={listName}
              listId={1}
              key={listName}
              listSubDescription='3/10 Completed'
              // eslint-disable-next-line no-console
              onDelete={() => console.log('onDelete')}
            />
            <Divider />
          </>
        )
      )}
    </List>
  </OverviewContainer>
)

export default ListsOverview
