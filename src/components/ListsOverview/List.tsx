import React from 'react'
import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
} from '@material-ui/core'
import DeleteIcon from '@material-ui/icons/Delete'

export interface ListProps {
  onDelete: (listId: number) => void
  listName: string
  listSubDescription: string
  listId: number
}

const List: React.FC<ListProps> = ({
  listName,
  listSubDescription,
  onDelete,
  listId,
}) => (
  <ListItem>
    <ListItemText primary={listName} secondary={listSubDescription} />
    <ListItemSecondaryAction>
      <IconButton edge='end' aria-label='delete'>
        <DeleteIcon onClick={() => onDelete(listId)} />
      </IconButton>
    </ListItemSecondaryAction>
  </ListItem>
)

export default List
