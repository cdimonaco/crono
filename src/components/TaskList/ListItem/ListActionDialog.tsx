import React from 'react'
import {
  makeStyles,
  Dialog,
  DialogTitle,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  createStyles,
} from '@material-ui/core'
import { blue } from '@material-ui/core/colors'
import TimerIcon from '@material-ui/icons/Timer'
import TimerOffIcon from '@material-ui/icons/TimerOff'
import DeleteIcon from '@material-ui/icons/Delete'

const useStyles = makeStyles(muiTheme =>
  createStyles({
    startTimeIcon: {
      backgroundColor: blue[200],
      color: blue[600],
    },
    deleteTaskIcon: {
      backgroundColor: muiTheme.palette.error.main,
    },
    dialogTitle: {
      paddingBottom: 'unset',
    },
  })
)

interface ListActionDialogProps {
  onClose: () => void
  open: boolean
}

const ListActionDialog: React.FC<ListActionDialogProps> = ({
  onClose,
  open,
}) => {
  const classes = useStyles()

  const handleClose = () => {
    onClose()
  }

  const handleListItemClick = () => {
    onClose()
  }

  return (
    <Dialog onClose={handleClose} aria-labelledby='task-dialog' open={open}>
      <DialogTitle className={classes.dialogTitle} id='task-dialog'>
        Calzini
      </DialogTitle>
      <List>
        <ListItem button onClick={() => handleListItemClick()}>
          <ListItemAvatar>
            <Avatar className={classes.startTimeIcon}>
              <TimerIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary='Start time tracking' />
        </ListItem>

        <ListItem autoFocus button onClick={() => handleListItemClick()}>
          <ListItemAvatar>
            <Avatar>
              <TimerOffIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary='End time tracking' />
        </ListItem>
        <ListItem autoFocus button onClick={() => handleListItemClick()}>
          <ListItemAvatar>
            <Avatar className={classes.deleteTaskIcon}>
              <DeleteIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText primary='Delete task' />
        </ListItem>
      </List>
    </Dialog>
  )
}

export default ListActionDialog
