import React from 'react'
import styled from '@emotion/styled'
import {
  List,
  ListItem,
  //   FormControlLabel,
  //   FormGroup,
  //   Checkbox,
  Switch,
  ListItemText,
  Divider,
  ListItemSecondaryAction,
  makeStyles,
} from '@material-ui/core'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import ListActionDialog from './ListItem/ListActionDialog'

const TaskListContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

const useStyles = makeStyles({
  itemActionGroup: {
    display: 'flex',
    alignItems: 'center',
  },
})

const TaskList: React.FC = () => {
  const classes = useStyles()
  const [dialogOpen, setDialogOpen] = React.useState(false)
  return (
    <>
      <TaskListContainer>
        <List>
          <ListItem>
            <ListItemText primary='Calzini' secondary='01/02/2020' />
            <ListItemSecondaryAction className={classes.itemActionGroup}>
              <Switch
                checked
                color='primary'
                value='checkedA'
                inputProps={{ 'aria-label': 'secondary checkbox' }}
              />
              <MoreVertIcon onClick={() => setDialogOpen(true)} />
            </ListItemSecondaryAction>
            <ListActionDialog
              open={dialogOpen}
              onClose={() => setDialogOpen(false)}
            />
          </ListItem>
          <Divider />
          <ListItem>
            <ListItemText primary='Pantaloni' secondary='01/02/2020' />
            <ListItemSecondaryAction className={classes.itemActionGroup}>
              <Switch
                checked
                color='primary'
                value='checkedA'
                inputProps={{ 'aria-label': 'secondary checkbox' }}
              />
              <MoreVertIcon onClick={() => setDialogOpen(true)} />
            </ListItemSecondaryAction>
            <ListActionDialog
              open={dialogOpen}
              onClose={() => setDialogOpen(false)}
            />
          </ListItem>
          <Divider />
          <ListItem>
            <ListItemText primary='Felpe' secondary='01/02/2020' />
            <ListItemSecondaryAction className={classes.itemActionGroup}>
              <Switch
                checked
                color='primary'
                value='checkedA'
                inputProps={{ 'aria-label': 'secondary checkbox' }}
              />
              <MoreVertIcon onClick={() => setDialogOpen(true)} />
            </ListItemSecondaryAction>
            <ListActionDialog
              open={dialogOpen}
              onClose={() => setDialogOpen(false)}
            />
          </ListItem>
          <Divider />
          <ListItem>
            <ListItemText primary='T Shirt' secondary='01/02/2020' />
            <ListItemSecondaryAction className={classes.itemActionGroup}>
              <Switch
                checked
                color='primary'
                value='checkedA'
                inputProps={{ 'aria-label': 'secondary checkbox' }}
              />
              <MoreVertIcon onClick={() => setDialogOpen(true)} />
            </ListItemSecondaryAction>
            <ListActionDialog
              open={dialogOpen}
              onClose={() => setDialogOpen(false)}
            />
          </ListItem>
          <Divider />
          <ListItem>
            <ListItemText primary='Shampoo' secondary='01/02/2020' />
            <ListItemSecondaryAction className={classes.itemActionGroup}>
              <Switch
                checked
                color='primary'
                value='checkedA'
                inputProps={{ 'aria-label': 'secondary checkbox' }}
              />
              <MoreVertIcon onClick={() => setDialogOpen(true)} />
            </ListItemSecondaryAction>
            <ListActionDialog
              open={dialogOpen}
              onClose={() => setDialogOpen(false)}
            />
          </ListItem>
          <Divider />
        </List>
      </TaskListContainer>
    </>
  )
}

export default TaskList
