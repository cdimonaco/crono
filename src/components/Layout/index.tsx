import React from 'react'
import Helmet from 'react-helmet'
import {
  createMuiTheme,
  MuiThemeProvider,
  makeStyles,
  Theme,
  createStyles,
} from '@material-ui/core'
import { indigo } from '@material-ui/core/colors'
import { GlobalInlcudes } from '../theme/GlobalIncludes'
import CronoAppBar from './AppBar'
import CronoDrawer from './Drawer'

const appTheme = createMuiTheme({
  palette: {
    primary: indigo,
  },
})

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    toolbar: theme.mixins.toolbar,
  })
)

// eslint-disable-next-line no-console
console.log(appTheme)
const Layout: React.FC = ({ children }) => {
  const classes = useStyles()
  const [mobileOpen, setMobileOpen] = React.useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen(prevState => !prevState)
  }

  return (
    <MuiThemeProvider theme={appTheme}>
      <GlobalInlcudes />
      <Helmet
        title='Crono'
        meta={[
          {
            name: 'description',
            content: 'desc',
          },
          { name: 'keywords', content: 'keyword' },
        ]}
      />
      <div className={classes.root}>
        <CronoAppBar
          mobileOpen={mobileOpen}
          handleDrawerToggle={handleDrawerToggle}
        />
        <CronoDrawer
          mobileOpen={mobileOpen}
          handleDrawerToggle={handleDrawerToggle}
        />
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {children}
        </main>
      </div>
    </MuiThemeProvider>
  )
}

export default Layout
