import React from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'

const drawerWidth = 240

const useStyles = (drawerWidthStatus: number) =>
  makeStyles((theme: Theme) =>
    createStyles({
      appBar: {
        [theme.breakpoints.up('md')]: {
          width: `calc(100% - ${drawerWidthStatus}px)`,
          marginLeft: drawerWidthStatus,
        },
      },
      mobileAppBar: {
        marginLeft: drawerWidthStatus,
      },
      menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
        color: theme.palette.getContrastText(theme.palette.primary.dark),
      },
      toolbar: theme.mixins.toolbar,
    })
  )

export interface CronoAppBarProps {
  handleDrawerToggle: () => void
  mobileOpen: boolean
}

const CronoAppBar: React.FC<CronoAppBarProps> = ({
  handleDrawerToggle,
  mobileOpen,
}) => {
  const classes = useStyles(drawerWidth)()

  return (
    <AppBar
      position='fixed'
      className={mobileOpen ? classes.mobileAppBar : classes.appBar}
    >
      <Toolbar>
        <IconButton
          aria-label='open drawer'
          edge='start'
          onClick={handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>
        <Typography variant='h6' noWrap>
          Overview
        </Typography>
      </Toolbar>
    </AppBar>
  )
}

export default CronoAppBar
