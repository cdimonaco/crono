import React from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import InsertInvitationIcon from '@material-ui/icons/InsertInvitation'
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import BookmarksIcon from '@material-ui/icons/Bookmarks'
import SettingsIcon from '@material-ui/icons/Settings'
import AccountBoxIcon from '@material-ui/icons/AccountBox'
import FavoriteIcon from '@material-ui/icons/Favorite'
import GitHubIcon from '@material-ui/icons/GitHub'

import {
  makeStyles,
  useTheme,
  Theme,
  createStyles,
} from '@material-ui/core/styles'
import { Typography, Collapse } from '@material-ui/core'
import DropdownItem from './DropdownItem'
// import { green } from '@material-ui/core/colors'

const drawerWidth = 240

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    drawer: {
      [theme.breakpoints.up('md')]: {
        width: drawerWidth,
        flexShrink: 0,
      },
    },
    toolbar: {
      ...theme.mixins.toolbar,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    drawerPaper: {
      width: drawerWidth,
      backgroundColor: theme.palette.primary.dark,
      color: theme.palette.getContrastText(theme.palette.primary.dark),
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    nested: {
      paddingLeft: theme.spacing(4),
    },
    bottomCredits: {
      marginBottom: 0,
      marginTop: 'auto',
    },
    bottomCreditsText: {
      flex: '0.4 0.4 auto',
    },
    drawerScrollContainer: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      height: '100%',
    },
    listItemIcon: {
      color: theme.palette.getContrastText(theme.palette.primary.dark),
    },
  })
)

interface ResponsiveDrawerProps {
  handleDrawerToggle: () => void
  mobileOpen: boolean
}

export default function ResponsiveDrawer(props: ResponsiveDrawerProps) {
  const classes = useStyles()
  const theme = useTheme()
  const [openCategories, setOpenCategories] = React.useState(false)
  const toggleCategories = () => {
    setOpenCategories(prevState => !prevState)
  }

  const drawer = (
    <div className={classes.drawerScrollContainer}>
      <div className={classes.toolbar}>
        <div>
          <Typography variant='h6'>Crono</Typography>
        </div>
      </div>
      <Divider />
      <List>
        <ListItem button>
          <ListItemIcon className={classes.listItemIcon}>
            <InsertInvitationIcon />
          </ListItemIcon>
          <ListItemText primary='Overview' />
        </ListItem>
        <ListItem button>
          <ListItemIcon className={classes.listItemIcon}>
            <AssignmentTurnedInIcon />
          </ListItemIcon>
          <ListItemText primary='Completed' />
        </ListItem>
        <ListItem button onClick={toggleCategories}>
          <ListItemIcon className={classes.listItemIcon}>
            <BookmarksIcon />
          </ListItemIcon>
          <ListItemText primary='Categories' />
          {openCategories ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={openCategories} timeout='auto' unmountOnExit>
          <List component='div' disablePadding>
            {['Work', 'Personal', 'Home', 'Finance', 'Finance', 'Finance'].map(
              nestedText => (
                <DropdownItem
                  nestedClassName={classes.nested}
                  listItemIconClassName={classes.listItemIcon}
                  listItemText={nestedText}
                  key={nestedText}
                />
              )
            )}
          </List>
        </Collapse>
      </List>
      <Divider />
      <List>
        <ListItem button>
          <ListItemIcon className={classes.listItemIcon}>
            <SettingsIcon />
          </ListItemIcon>
          <ListItemText primary='Settings' />
        </ListItem>
        <ListItem button>
          <ListItemIcon className={classes.listItemIcon}>
            <AccountBoxIcon />
          </ListItemIcon>
          <ListItemText primary='Profile' />
        </ListItem>
      </List>
      <Divider />
      <div className={classes.bottomCredits}>
        <Divider />
        <List disablePadding dense>
          <ListItem button>
            <ListItemText
              className={classes.bottomCreditsText}
              primary='Made with'
            />
            <ListItemIcon className={classes.listItemIcon}>
              <FavoriteIcon />
            </ListItemIcon>
          </ListItem>
          <ListItem button>
            <ListItemText
              className={classes.bottomCreditsText}
              primary='Get source'
            />
            <ListItemIcon className={classes.listItemIcon}>
              <GitHubIcon />
            </ListItemIcon>
          </ListItem>
        </List>
      </div>
    </div>
  )

  return (
    <div className={classes.root}>
      <CssBaseline />
      <nav className={classes.drawer} aria-label='mailbox folders'>
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation='css'>
          <Drawer
            variant='temporary'
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={props.mobileOpen}
            onClose={props.handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation='css'>
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant='permanent'
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </div>
  )
}
