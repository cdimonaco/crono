import React from 'react'
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import LabelImportantIcon from '@material-ui/icons/LabelImportant'

export interface DropdownItemProps {
  nestedClassName: string
  listItemIconClassName: string
  listItemText: string
}
const DropdownItem: React.FC<DropdownItemProps> = ({
  nestedClassName,
  listItemIconClassName,
  listItemText,
}) => (
  <ListItem button className={nestedClassName}>
    <ListItemIcon className={listItemIconClassName}>
      <LabelImportantIcon />
    </ListItemIcon>
    <ListItemText primary={listItemText} />
  </ListItem>
)

export default DropdownItem
