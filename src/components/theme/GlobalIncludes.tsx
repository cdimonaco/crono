import React from 'react'
import { Global } from '@emotion/core'
import { spacingsCSS } from './css/spacingsCSS'
import { resetCSS } from './css/resetCSS'
import { headingsCSS } from './css/headingsCSS'

// Be sure to load HelmetProvider on top of your App
export const GlobalInlcudes = () => (
  <>
    <Global styles={spacingsCSS} />
    <Global styles={resetCSS} />
    <Global styles={headingsCSS} />
  </>
)
