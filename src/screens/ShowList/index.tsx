import React from 'react'
import TaskList from 'src/components/TaskList'
import styled from '@emotion/styled'
import { theme } from 'src/components/theme'
import { Hidden, Typography } from '@material-ui/core'

const ListTitleContainer = styled.div`
  padding: ${theme.spacings.sm};
`

const ShowList: React.FC = () => (
  <>
    <ListTitleContainer>
      <Hidden mdUp>
        <Typography variant='h6'>Fosdem 2020</Typography>
      </Hidden>
      <Hidden smDown>
        <Typography variant='h3'>Fosdem 2020</Typography>
      </Hidden>
    </ListTitleContainer>
    <TaskList />
  </>
)

export default ShowList
