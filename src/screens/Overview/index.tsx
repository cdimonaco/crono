import React from 'react'
import ListsOverview from 'src/components/ListsOverview'

const Overview: React.FC = () => (
  <>
    <ListsOverview />
  </>
)

export default Overview
