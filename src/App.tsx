import React from 'react'
import { Switch, Route, BrowserRouter } from 'react-router-dom'
import Layout from './components/Layout'
import Overview from './screens/Overview'
import ShowList from './screens/ShowList'

const App: React.FC = () => (
  <Layout>
    <BrowserRouter>
      <Switch>
        <Route exact path='/' component={Overview} />
        <Route exact path='/:id' component={ShowList} />
      </Switch>
    </BrowserRouter>
  </Layout>
)

export default App
